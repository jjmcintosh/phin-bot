
import tweepy 
import config
import time
import random
import re
from apscheduler.schedulers.blocking import BlockingScheduler

sched = BlockingScheduler()


api_m = tweepy.Client(
    bearer_token = config.bearer_token,
    consumer_key = config.consumer_key,
    consumer_secret = config.consumer_secret,
    access_token = config.access_token,
    access_token_secret = config.access_token_secret
    )


bot_id = 1581839438363168769

bot_name = "@PhineasFlynnBot"

#@sched.scheduled_job('cron', day_of_week='sun-sat', hour=9)
@sched.scheduled_job('interval', minutes=3)
def FerbTweet():

    print("Sending Tweet!")
    message= "@Ferb, I know what we're going to do today!"
    api_m.create_tweet(text = message)

@sched.scheduled_job('cron', day_of_week='sun-sat', hour=10)
def WhereIsPerry():

    print("Sending Tweet!")
    message= random.choice(["Hey, where's Perry?", "Hey, has anyone seen Perry?", "Hmm, where's Perry?"])
    api_m.create_tweet(text = message)

@sched.scheduled_job('cron', day_of_week='sun-sat', hour=17)
def ThereIsPerry():

    print("Sending Tweet!")
    message= "There you are, Perry."
    api_m.create_tweet(text = message)


sched.start()







